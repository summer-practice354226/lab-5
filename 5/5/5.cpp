﻿#include <iostream>
#include <cmath>
#include <ctime>

using namespace std;

void inputArray(double* arr, int n) {
    cout << "Enter the elements of the array:" << endl;
    for (int i = 0; i < n; i++) {
        cin >> arr[i];
    }
}

void printArray(double* arr, int n) {
    cout << "Original array: ";
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

void sortOddPositions(double* arr, int n, int a) {
    for (int i = 0; i < n - 1; i += 2) {
        for (int j = i + 2; j < n; j += 2) {
            if ((int)arr[i] % a < (int)arr[j] % a) {
                swap(arr[i], arr[j]);
            }
        }
    }
}

void createRoundedArray(double* arr, int n, int k, double*& new_arr, int& unique_count) {
    unique_count = 0;
    new_arr = new double[n];
    for (int i = 0; i < n; i++) {
        new_arr[i] = round(arr[i] * pow(10, k)) / pow(10, k);
    }

    for (int i = 0; i < n; i++) {
        bool is_unique = true;
        for (int j = 0; j < i; j++) {
            if (new_arr[i] == new_arr[j]) {
                is_unique = false;
                break;
            }
        }
        if (is_unique) {
            new_arr[unique_count++] = new_arr[i];
        }
    }
}

void printSortedArray(double* arr, int n) {
    cout << "Sorted and rounded array: ";
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main() {
    int n;
    cout << "Enter the size of the array: ";
    cin >> n;
    double* arr = new double[n];
    inputArray(arr, n);
    printArray(arr, n);

    int a;
    cout << "Enter the value of a: ";
    cin >> a;

    sortOddPositions(arr, n, a);

    int k;
    cout << "Enter the value of k: ";
    cin >> k;

    double* new_arr;
    int unique_count;
    createRoundedArray(arr, n, k, new_arr, unique_count);

    printSortedArray(new_arr, unique_count);

    delete[] arr;
    delete[] new_arr;

    return 0;
}